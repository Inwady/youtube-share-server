from test.ok.Page import Page

from selenium.webdriver.common.keys import Keys

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.support import expected_conditions as EC

class LoginPage(Page):
    FORM_ID = "form-login"

    SIGNIN_BUTTON_ID = "identifierNext"
    PASSWORD_NEXT_BUTTON = "passwordNext"

    REGISTER_BUTTON_CLASS = "Register-button"

    INPUT_LOGIN     = 'identifier'
    INPUT_PASSWORD  = 'password'

    # TODO fix
    BUTTON_CLASS = '//span[text()="Login"]'

    IMAGE_WAIT_CLASS = 'logo'

    def __init__(self, driver, login, password):
        super(LoginPage, self).__init__(driver, "login_page")

        self.login = login
        self.password = password

    def auth(self):
        button = self.driver.find_element_by_id(self.SIGNIN_BUTTON_ID)

        login_field = self.driver.find_element_by_name(self.INPUT_LOGIN)

        login_field.send_keys(self.login)

        button.click()

        button = self.wait_next_button()

        password_field = self.driver.find_element_by_name(self.INPUT_PASSWORD)
        password_field.send_keys(self.password)

        button.click()

        self.wait_success()

        # base = self.driver.find_element_by_id(self.FORM_ID)
        # button = base.find_element_by_xpath(self.BUTTON_CLASS)
        #
        # self.driver.execute_script("arguments[0].click();", button)
        #
        # self.wait_success()

    def wait_success(self):
        waitShowPanel = lambda s: s.find_element_by_class_name(self.IMAGE_WAIT_CLASS)
        self.wait.until(waitShowPanel)

    def wait_next_button(self):
        self.wait.until(EC.element_to_be_clickable((By.ID, self.PASSWORD_NEXT_BUTTON)))
        return self.driver.find_element_by_id(self.PASSWORD_NEXT_BUTTON)
