from test.cases.Case import Case

import random
from hamcrest import *

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from config import config

from test.ok.LoginPage import LoginPage


class LoginTestCase(Case):
    def testSimpleLogin(self):
        super(LoginTestCase, self).setUp()

        page = LoginPage(self.driver,
                         config["user.login"],
                         config["user.password"])

        page.open("/edit?video_id=" + config["youtube.share.video_id"])
        page.auth()
