# -*- coding: utf-8 -*-

import unittest
import sys

from test.cases.LoginTestCase import LoginTestCase

if __name__ == '__main__':
    login_test = unittest.TestLoader().loadTestsFromTestCase(LoginTestCase)

    suite = unittest.TestSuite([
        login_test,
    ])

    result = unittest.TextTestRunner().run(suite)
    sys.exit(not result.wasSuccessful())
