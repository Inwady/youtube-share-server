```
# default ./config/codechat.yaml
python run_tests.py [-c config]
```

export PATH=$PATH:$(pwd)
python run_tests.py -c config/youtube.yaml

# Save session

```
import pickle
import selenium.webdriver 

driver = selenium.webdriver.Firefox()
driver.get("http://www.google.com")
cookies = pickle.load(open("cookies.pkl", "rb"))
for cookie in cookies:
    driver.add_cookie(cookie)

```